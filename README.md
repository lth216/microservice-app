<!-- <p align="center">
<img src="/src/frontend/static/icons/Hipster_HeroLogoMaroon.svg" width="300" alt="Online Boutique" />
</p> -->
![Continuous Integration](https://github.com/GoogleCloudPlatform/microservices-demo/workflows/Continuous%20Integration%20-%20Main/Release/badge.svg)

**Online Boutique** is a cloud-first microservices demo application.  The application is a
web-based e-commerce app where users can browse items, add them to the cart, and purchase them.

Google uses this application to demonstrate how developers can modernize enterprise applications using Google Cloud products, including: [Google Kubernetes Engine (GKE)](https://cloud.google.com/kubernetes-engine), [Anthos Service Mesh (ASM)](https://cloud.google.com/service-mesh), [gRPC](https://grpc.io/), [Cloud Operations](https://cloud.google.com/products/operations), [Spanner](https://cloud.google.com/spanner), [Memorystore](https://cloud.google.com/memorystore), [AlloyDB](https://cloud.google.com/alloydb), and [Gemini](https://ai.google.dev/). This application works on any Kubernetes cluster.

If you’re using this demo, please **★Star** this repository to show your interest!

**Note to Googlers:** Please fill out the form at [go/microservices-demo](http://go/microservices-demo).

## Architecture

**Online Boutique** is composed of 11 microservices written in different
languages that talk to each other over gRPC.

[![Architecture of
microservices](/docs/img/architecture-diagram.png)](/docs/img/architecture-diagram.png)

Find **Protocol Buffers Descriptions** at the [`./protos` directory](/protos).

| Service                                              | Language      | Description                                                                                                                       |
| ---------------------------------------------------- | ------------- | --------------------------------------------------------------------------------------------------------------------------------- |
| [frontend](/src/frontend)                           | Go            | Exposes an HTTP server to serve the website. Does not require signup/login and generates session IDs for all users automatically. |
| [cartservice](/src/cartservice)                     | C#            | Stores the items in the user's shopping cart in Redis and retrieves it.                                                           |
| [productcatalogservice](/src/productcatalogservice) | Go            | Provides the list of products from a JSON file and ability to search products and get individual products.                        |
| [currencyservice](/src/currencyservice)             | Node.js       | Converts one money amount to another currency. Uses real values fetched from European Central Bank. It's the highest QPS service. |
| [paymentservice](/src/paymentservice)               | Node.js       | Charges the given credit card info (mock) with the given amount and returns a transaction ID.                                     |
| [shippingservice](/src/shippingservice)             | Go            | Gives shipping cost estimates based on the shopping cart. Ships items to the given address (mock)                                 |
| [emailservice](/src/emailservice)                   | Python        | Sends users an order confirmation email (mock).                                                                                   |
| [checkoutservice](/src/checkoutservice)             | Go            | Retrieves user cart, prepares order and orchestrates the payment, shipping and the email notification.                            |
| [recommendationservice](/src/recommendationservice) | Python        | Recommends other products based on what's given in the cart.                                                                      |
| [adservice](/src/adservice)                         | Java          | Provides text ads based on given context words.                                                                                   |
| [loadgenerator](/src/loadgenerator)                 | Python/Locust | Continuously sends requests imitating realistic user shopping flows to the frontend.                                              |

## Screenshots

| Home Page                                                                                                         | Checkout Screen                                                                                                    |
| ----------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------ |
| [![Screenshot of store homepage](/docs/img/online-boutique-frontend-1.png)](/docs/img/online-boutique-frontend-1.png) | [![Screenshot of checkout screen](/docs/img/online-boutique-frontend-2.png)](/docs/img/online-boutique-frontend-2.png) |

## Quickstart (K8s)

1. Ensure you have the following requirements:
   - Shell environment with `git`, `kubectl`, `helm`

2. Clone the latest major version.

   ```sh
   git clone --depth 1 --branch v0 https://github.com/lth216/microservices-shopping-app.git
   cd microservices-shopping-app/
   ```

   The `--depth 1` argument skips downloading git history.

3. (Optional) In case your cluster doesn't have Load Balancer, you can remove Certificate and Ingress section in manifests/frontend.yaml and use NodePort service instead and skip **step 4** and **step 5**
   

4. If you don't have Cert-Manager addon with self-signed certificate ClusterIssuer. Please remove Certificate and tls field in manifests/frontend.yaml
   ```sh
   #Remove this in manifest/frontend.yaml
   apiVersion: cert-manager.io/v1
   kind: Certificate
   metadata:
   name: frontend-cert
   namespace: shopping-app
   spec:
   issuerRef:
      name: selfsigned-cluster-issuer
      kind: ClusterIssuer
   secretName: frontend-cert-secret
   dnsNames:
   - longtruong-lab.online

   # Remove tls field in Ingress also
   ...
   spec:
      tls:
      - hosts:
         - longtruong-lab.online
         secretName: frontend-cert-secret
   ...
   ```

5. Replace DNS host in Ingress with yours
   ```sh
   apiVersion: networking.k8s.io/v1
   kind: Ingress
   metadata:
      name: frontend-ingress
      namespace: shopping-app
   spec:
   #In case you wish to use HTTPS
   tls:
   - hosts:
      - #<your-DNS>
      secretName: frontend-cert-secret
   ingressClassName: nginx
   rules:
      - host: #<your-DNS>
         http:
         paths:
         - path: /
            pathType: Prefix
            backend:
               service:
               name: frontend-svc
               port:
                  number: 8080
   ```

6. Deploy Online Boutique to the cluster.

   ```sh
   kubectl apply -f manifests/namespace.yaml
   kubectl apply -f manifests/
   ```

7. Wait for the pods to be ready.

   ```sh
   kubectl get pods -n shopping-app
   ```

   After a few minutes, you should see the Pods in a `Running` state:

   ```
   NAME                                        READY   STATUS    RESTARTS   AGE
   adservice-dp-6bdd6fdf85-xfkpt               1/1     Running   0          112s
   cartservice-dp-5f846d76bd-4pzd7             1/1     Running   0          112s
   checkoutservice-dp-557db58db8-z6bqv         1/1     Running   0          112s
   currencyservice-dp-75779769d-fqf2v          1/1     Running   0          111s
   emailservice-dp-58c4559ff5-tmtcj            1/1     Running   0          111s
   frontend-dp-5fd475cd4d-8qrfr                1/1     Running   0          111s
   paymentservice-dp-7dd8768bc7-7sq96          1/1     Running   0          111s
   productcatalogservice-dp-769b985c7c-2fgzb   1/1     Running   0          111s
   recommendationservice-dp-7d6bc4d9f5-ptvhf   1/1     Running   0          111s
   rediscart-dp-544cbfc47d-2rqwz               1/1     Running   0          112s
   shippingservice-dp-947564b85-ch4bv          1/1     Running   0          111s
   ```

8. Access the web frontend in a browser using the frontend's DNS (Ingress) or `<NodeIP>:<NodePort>`.


## Documentation

- [Development](/docs/development-guide.md) to learn how to run and develop this app locally.

## Demos featuring Online Boutique

- [Platform Engineering in action: Deploy the Online Boutique sample apps with Score and Humanitec](https://medium.com/p/d99101001e69)
- [The new Kubernetes Gateway API with Istio and Anthos Service Mesh (ASM)](https://medium.com/p/9d64c7009cd)
- [Use Azure Redis Cache with the Online Boutique sample on AKS](https://medium.com/p/981bd98b53f8)
- [Sail Sharp, 8 tips to optimize and secure your .NET containers for Kubernetes](https://medium.com/p/c68ba253844a)
- [Deploy multi-region application with Anthos and Google cloud Spanner](https://medium.com/google-cloud/a2ea3493ed0)
- [Use Google Cloud Memorystore (Redis) with the Online Boutique sample on GKE](https://medium.com/p/82f7879a900d)
- [Use Helm to simplify the deployment of Online Boutique, with a Service Mesh, GitOps, and more!](https://medium.com/p/246119e46d53)
- [How to reduce microservices complexity with Apigee and Anthos Service Mesh](https://cloud.google.com/blog/products/application-modernization/api-management-and-service-mesh-go-together)
- [gRPC health probes with Kubernetes 1.24+](https://medium.com/p/b5bd26253a4c)
- [Use Google Cloud Spanner with the Online Boutique sample](https://medium.com/p/f7248e077339)
- [Seamlessly encrypt traffic from any apps in your Mesh to Memorystore (redis)](https://medium.com/google-cloud/64b71969318d)
- [Strengthen your app's security with Anthos Service Mesh and Anthos Config Management](https://cloud.google.com/service-mesh/docs/strengthen-app-security)
- [From edge to mesh: Exposing service mesh applications through GKE Ingress](https://cloud.google.com/architecture/exposing-service-mesh-apps-through-gke-ingress)
- [Take the first step toward SRE with Cloud Operations Sandbox](https://cloud.google.com/blog/products/operations/on-the-road-to-sre-with-cloud-operations-sandbox)
- [Deploying the Online Boutique sample application on Anthos Service Mesh](https://cloud.google.com/service-mesh/docs/onlineboutique-install-kpt)
- [Anthos Service Mesh Workshop: Lab Guide](https://codelabs.developers.google.com/codelabs/anthos-service-mesh-workshop)
- [KubeCon EU 2019 - Reinventing Networking: A Deep Dive into Istio's Multicluster Gateways - Steve Dake, Independent](https://youtu.be/-t2BfT59zJA?t=982)
- Google Cloud Next'18 SF
  - [Day 1 Keynote](https://youtu.be/vJ9OaAqfxo4?t=2416) showing GKE On-Prem
  - [Day 3 Keynote](https://youtu.be/JQPOPV_VH5w?t=815) showing Stackdriver
    APM (Tracing, Code Search, Profiler, Google Cloud Build)
  - [Introduction to Service Management with Istio](https://www.youtube.com/watch?v=wCJrdKdD6UM&feature=youtu.be&t=586)
- [Google Cloud Next'18 London – Keynote](https://youtu.be/nIq2pkNcfEI?t=3071)
  showing Stackdriver Incident Response Management